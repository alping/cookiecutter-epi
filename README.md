# Cookiecutter Epi

```shell
# pip install cookiecutter
cookiecutter https://gitlab.com/alping/cookiecutter-epi
```

## Setup Data

If the script could not create symlinks to remote data automatically, this can be done manually:

```batch
: On Windows
mklink \D data/raw <target_path>/raw
mklink \D data/interim <target_path>/interim
mklink \D data/processed <target_path>/processed
mklink \D data/external <target_path>/external
```

<!--
Fingerprint data and store in data.yaml

```batch
snakemake hash_data
```
-->
