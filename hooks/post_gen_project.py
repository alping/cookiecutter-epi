import sys
from pathlib import Path

data_path = r"{{ cookiecutter.data_path }}"
local_path = Path("data")
data_types = ["raw", "interim", "processed", "external"]

if data_path:
    data_root = Path(data_path)
    data_dir = data_root.joinpath("{{ cookiecutter.project_slug }}")

    # Check that data_root exists and that data_dir does not exist or is empty,
    # otherwise exit with code 1
    try:
        assert data_root.is_dir(), f'"{data_root}" does not exist or is not a directory'
        assert not list(
            data_dir.rglob("*")
        ), f'"{data_dir}" already exists and is not empty'
    except AssertionError as e:
        sys.exit(e)

    # Create remote directories
    for d in data_types:
        data_dir.joinpath(d).mkdir(parents=True)

    # Create local symlinks
    try:
        for d in data_types:
            local_path.joinpath(d).symlink_to(
                data_dir.joinpath(d), target_is_directory=True
            )
    except OSError as e:
        print(f'Failed to create symlinks: "{e}"')
        print("See documentation for manual generation of symlinks")
else:
    # Create local directories
    for d in data_types:
        local_path.joinpath(d).mkdir()
